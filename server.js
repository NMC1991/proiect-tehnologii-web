const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

const sequelize = new Sequelize('c9','root','',{
	dialect : 'mysql'
	
});

const Category = sequelize.define('category', {
	categoryName : Sequelize.STRING
});

const Eveniment = sequelize.define('eveniment', {
	evenimentName : Sequelize.STRING,
	evenimentLocation : Sequelize.STRING,
	evenimentPrice: Sequelize.INTEGER
});

Category.hasMany(Eveniment, {foreignKey: 'categoryId'});
Eveniment.belongsTo(Category, {foreignKey: 'categoryId'});


const aux = express();
aux.use(bodyParser.json());

aux.get('/create', (req, res) => {
	sequelize.sync({force : true})
		.then(() => res.status(201).send('recreated all tables'))
		.catch(() => res.status(500).send('hm, that was unexpected...'));	
});

aux.get('/categories', (req, res) => {
	Category.findAll()
		.then((results) => {
			res.status(200).json(results);
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));			
});

aux.post('/categories', (req, res) => {
	Category.create(req.body)
		.then(() => {
			res.status(201).send('created');
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));
});

aux.get('/categories/:id', (req, res) => {
	Category.findById(req.params.id)
		.then((result) => {
			if (result){
				res.status(200).json(result);
			}
			else{
				res.status(404).send('not found');
			}
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));
});

aux.put('/categories/:id', (req, res) => {
	Category.findById(req.params.id)
		.then((result) => {
			if (result){
				return result.update(req.body);
			}
			else{
				res.status(404).send('not found');
			}
		})
		.then(() => {
			res.status(201).send('modified');
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));
});

aux.delete('/categories/:id', (req, res) => {
	Category.findById(req.params.id)
		.then((result) => {
			if (result){
				return result.destroy();
			}
			else{
				res.status(404).send('not found');	
			}
		})
		.then(() => {
			res.status(201).send('removed');
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));
});

aux.get('/categories/:cid/evenimente', (req, res) => {
	Category.findById(req.params.cid)
		.then((result) => {
			if (result){
				return result.getChapters();
			}
			else{
				res.status(404).send('not found');	
			}
		})
		.then((results) => {
			res.status(200).json(results);
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));
});

aux.get('/categories/:cid/evenimente/:sid', (req, res) => {
	Category.findById(req.params.cid)
		.then((result) => {
			if (result){
				return result.getEvenimente({where : {id : req.params.sid}});
			}
			else{
				res.status(404).send('not found');	
			}
		})
		.then((result) => {
			if (result){
				res.status(200).json(result);
			}
			else{
				res.status(404).send('not found');
			}
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));	
});

aux.post('/categories/:cid/evenimente', (req, res) => {
	Category.findById(req.params.cid)
		.then((result) => {
			if (result){
				let eveniment = req.body;
				eveniment.categoryId = result.id;
				return Eveniment.create(eveniment);
			}
			else{
				res.status(404).send('not found');	
			}
		})
		.then(() => {
			res.status(201).json('created');
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));
});

aux.put('/categories/:cid/evenimente/:sid', (req, res) => {
	Eveniment.findById(req.params.sid)
		.then((result) => {
			if (result){
				return result.update(req.body);
			}
			else{
				res.status(404).send('not found');	
			}
		})
		.then(() => {
			res.status(201).send('modified');
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));
});

aux.delete('/categories/:cid/evenimente/:sid', (req, res) => {
	Eveniment.findById(req.params.sid)
		.then((result) => {
			if (result){
				return result.destroy();
			}
			else{
				res.status(404).send('not found');	
			}
		})
		.then(() => {
			res.status(201).send('removed');
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));
});

aux.listen(8080);
console.log("App listening on port 8080");